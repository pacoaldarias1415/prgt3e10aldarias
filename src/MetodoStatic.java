
/**
 * Fichero: MetodoStatic.java
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 04-nov-2013
 */
public class MetodoStatic {

  int a = 1;
  static int b = 2;

  static void funcionStatic() {
    // System.out.println(a); // Error 
    System.out.println(b);
  }

  void funcion() {
    System.out.println(b);  
    System.out.println(a);
  }

  public static void main(String[] args) {
    MetodoStatic p = new MetodoStatic();
    funcionStatic();
    p.funcion();
    System.out.println(b);
    // System.out.println(a); // Error
  }
}

/* Ejecición
2
2
1
2
*/