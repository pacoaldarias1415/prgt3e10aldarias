
import java.io.BufferedReader;
import java.io.InputStreamReader;



/**
 * Fichero: LeeLinea.java
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 28-oct-2013
 */

public class LeeLinea {
  public static void main(String[] args ) {
    InputStreamReader input = new InputStreamReader(System.in);
    BufferedReader buffer = new BufferedReader(input);
    try {
      System.out.print("Introduce Linea: ");
      String linea = buffer.readLine();
      System.out.println("Linea leida: "+linea);
    }
    catch ( Exception e) {
    }
  }

}
/* EJECUCION:
Introduce Linea: aaaaaaaaaaaa
Linea leida: aaaaaaaaaaaa
*/
